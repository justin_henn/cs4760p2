//main
//Thi is the main file and the master file for the master/slave program
//Justin Henn
//2/19/17
//Assignment 2
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#define PERM (S_IRUSR | S_IWUSR)

int* point;
int num_proc_for_alarm;

void ALARM_handler(int sig) {//Argument here is signal number

  int y;
  fprintf(stderr, "Reached alarm\n");  
  for(y = 0; y < num_proc_for_alarm; y++)
    kill(point[y], SIGKILL);
  //printf("HI \n");
} 

void SIGINT_handler(int sig) {
  signal(sig, SIG_IGN);
  printf("Received Control C\n");

  //signal(sig, SIGINT_handler);
}

/*void CHILD_handler(int sig) {

  fprintf(stderr,"idk\n");
}*/
 
int detachandremove(int shmid, void *shmaddr) {
   int error = 0; 

   if (shmdt(shmaddr) == -1)
     error = errno;
   if ((shmctl(shmid, IPC_RMID, NULL) == -1) && !error)
     error = errno;
   if (!error)
      return 0;
   return -1;
}


int main (int argc, char **argv) {


  //char* argument = argv[0];
  int opt;
  char* filename = NULL;
  int number_proc = 0, number_increment = 0, number_wait = 0, i, shm_id_total, shm_id_choosing, shm_id_number, x;
  int* sharedNum;
  int* choosing;
  int* number;
  extern char* optarg;
  key_t key_total, key_choosing, key_number;
  key_total = 3423563;
  key_choosing = 12345;
  key_number = 56789;

  while ((opt = getopt(argc, argv, "hs:l:i:t:")) != -1) { //this loop looks at the the arguments and processes them appropriately

    switch (opt) {
    case 'h':
      printf("h - help\nn - specify a value after n for the error code \nl - specify a file name after l for the logfile \ns - number of slave processes \ni - times slave increments \nt - seconds when program should terminate\n");
      return 0;
    case 's':
      number_proc = atoi(optarg);
      break;
    case 'l':
      filename = optarg;
      break;
    case 'i':
      number_increment  = atoi(optarg);
      break;
    case 't':
      number_wait  = atoi(optarg);
      break;
    }
  }

  if (filename == NULL) { //if no filename was givien as an argument

     filename = "test.out";
  }

  if (number_proc == 0) { //if no number of processes was given as argument

    number_proc = 4;
  }
  if (number_increment == 0) { //if no number of increments was given as argument

    number_increment = 3;
  }
  if (number_wait == 0) { //if no wait time was given as argument

    number_wait = 20;
  }
  
  //get shared memory for sharedNum
  if ((shm_id_total = shmget(key_total, sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((sharedNum = (int *)shmat(shm_id_total, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_total, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
   }

  //get shared memory for choosing   
  if ((shm_id_choosing = shmget(key_choosing, number_proc*sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((choosing = shmat(shm_id_choosing, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_choosing, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }
   
  //get shared memory for number  
  if ((shm_id_number = shmget(key_number, number_proc*sizeof(int), PERM | IPC_CREAT)) == -1) {
    perror("FAIL");
    return 1;
  }
  if ((number = shmat(shm_id_number, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    if (shmctl(shm_id_number, IPC_RMID, NULL) == -1)
      perror("Failed to  remove memory segment");
      return 1;
  }
  
 //initialize the shared memory with 0
  for (x = 0; x < number_proc; x++) {
       
       choosing[x] = 0;
       number[x] = 0;
  }  
  *sharedNum = 0;

  //printf("%d, %d, %d\n", number_proc, number_increment, number_wait);

  pid_t pids[number_proc];
  pid_t child_pid;

  //create processes
  for(i=0; i<number_proc; i++) {
   char str[2];
   char str1[5];
   char str2[5];
   sprintf(str, "%d", i);
   sprintf(str1, "%d", number_increment);
   sprintf(str2, "%d", number_proc);
   child_pid = fork();

  //couldnt fork
    if (child_pid < 0) {

      printf("Could not fork!");
      return 1;
    }

   //Create array of process ids so can parent process can wait for all processes
    else if (child_pid > 0) {
      pids[i] = child_pid;
      point = pids;
      num_proc_for_alarm = number_proc;

    }
  
  //child process
    else  {
  //  printf("in child %d\n", getpid());

      //signal(SIGINT, CHILD_handler);

       //execl("./slave", "slave", "-x", str, 0);
      execl("./slave", "slave", str, filename, str1, str2, 0);
    }   
  }
  //printf("Main Process\n");

  //Parent process waiting for child processes to complete

  signal(SIGINT, SIGINT_handler);
  signal(SIGALRM, ALARM_handler);
  alarm(number_wait);  
  for (i=0; i < number_proc; i++) {
    int status;
    wait(pids[i], &status, 0);
    //printf("Process %d finished\n", pids[i]);
  }
  
  //detach and remove all shared memory
  if (detachandremove(shm_id_total, sharedNum) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }
  if (detachandremove(shm_id_choosing, (void *)choosing) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }
  if (detachandremove(shm_id_number, number) == -1) {
    perror("Failed to destroy shared memory segment");
    return 1;
  }
  return 0;
}
