//salve
////This is the slave file for the master/slave program
////Justin Henn
////2/20/17
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#define PERM (S_IRUSR | S_IWUSR)

 //get the maximum of the number array
int max_array(int a[], int num_elements) {

   int i, max = -1;
   for (i=0; i<num_elements; i++)
   {
     if (a[i]>max)
     {
        max=a[i];
     }
   }
   return(max);


}


int main (int argc, char **argv) {

  key_t key_total, key_choosing, key_number;
  key_total = 3423563;
  key_choosing = 12345;
  key_number = 56789;
  int* sharedNum;
  int* choosing;
  int* number;
  int proc_num = atoi(argv[1]);
  int num_incr = atoi(argv[3]);
  int number_of_proc = atoi(argv[4]);
  extern char* optarg;
  int opt;
  int shm_id_total, i, shm_id_choosing, shm_id_number, j;
  FILE* logfile;
  struct timespec tps;
  srand (time(NULL)); 
  
/*  while ((opt = getopt(argc, argv, "x:")) != -1) {
    
    switch (opt) {
    case 'x':
      proc_num = atoi(optarg);
      break;
    }
}*/

// printf("%s\n", argv[3]); 
  

  //get shared memory that was created in master
  if ((shm_id_total = shmget(key_total, sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((sharedNum = (int *)shmat(shm_id_total, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    return 1;
  }
  
  
  if ((shm_id_choosing = shmget(key_choosing, 4* sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((choosing = (int *)shmat(shm_id_choosing, NULL, 0)) == (void *)-1) {
      perror("Failed to attach shared memory segment");
      return 1;
   }
  

  if ((shm_id_number = shmget(key_number, 4*sizeof(int), PERM)) == -1) {
    perror("shmget");
    return 1;
  }
  if ((number = (int *)shmat(shm_id_number, NULL, 0)) == (void *)-1) {
    perror("Failed to attach shared memory segment");
    return 1;
  }

  //bakery algorithm
   
  for (i = 0; i < num_incr; i++) {
    if (clock_gettime(CLOCK_REALTIME, &tps) != 0) {
       perror("Error in getting time");
       return -1;
    }
    fprintf(stderr,"Execute code to enter critical section at time %lu%lu for pid %d\n", tps.tv_sec, tps.tv_nsec, getpid());
    choosing[proc_num] = 1;
    number[proc_num] = 1 + max_array(number, number_of_proc);
    choosing[proc_num] = 0;
    for (j = 0; j < number_of_proc; j++)
      if (j != proc_num) {
        while (choosing[j]) ;
        while (number[j] != 0 && (number[j] < number[proc_num] || (number[j] == number[proc_num] && j < proc_num))) ;
      }
    if (clock_gettime(CLOCK_REALTIME, &tps) != 0) {
       perror("Error in getting time");
       return -1;
    }
    fprintf(stderr, "Process %d Enter Critical at time %lu%lu\n", getpid(), tps.tv_sec, tps.tv_nsec);
    if ((logfile = fopen(argv[2], "a+")) == NULL) { //check to see if it opened a file

      perror("File open error");
      return -1;
    }
     sleep((rand() % 3));
     *sharedNum = *sharedNum + 1;
     if (clock_gettime(CLOCK_REALTIME, &tps) != 0) {
       perror("Error in getting time");
       return -1;
     }
     sleep((rand() % 3));
     fprintf(logfile,"File modified by process number %d, at time %lu%lu with sharedNum = %d\n", getpid(), tps.tv_sec, tps.tv_nsec, *sharedNum);
     fflush(logfile);
     fclose(logfile);
     number[proc_num] = 0;
     if (clock_gettime(CLOCK_REALTIME, &tps) != 0) {
       perror("Error in getting time");
       return -1;
     }
     fprintf(stderr, "Process %d Exit Critical at time %lu%lu\n", getpid(), tps.tv_sec, tps.tv_nsec);
  }
  /*if (shmdt(sharedNum) == -1)
     printf("shmdt");
  if (shmdt(choosing) == -1)
     printf("shmdt");
  if (shmdt(number) == -1)
     printf("shmdt"); */
   

return 0;

 
}
