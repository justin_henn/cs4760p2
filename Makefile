CC	= gcc		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET = master slave

all: master slave

master:	main.c
	$(CC) $(CFLAGS) -o master main.c

slave: slave.c
	$(CC) $(CFLAGS) -o slave slave.c

clean:
	/bin/rm -f *.o *~ *.txt *.out $(TARGET)
